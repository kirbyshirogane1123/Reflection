Head First C# 深入淺出C#

這本書算是蠻特別的一本書，裡面的圖片比文字還要多，
就個人認為算是蠻好理解的，我個人算有實務經驗但是在基礎上算是比較不是那麼理解，
這本書還蠻好上手的，但裡面的作業系統跟我現在的不同，
所以有些範例沒辦法直接運用，但稍微改一下就可以就能在系統上運行了。

雖然這本書的圖片非常非常多，
但其實裡面該講的都有講解，並且有非常多的小註解可以讓閱讀者能夠更快速的理解，
包含：物件導向、型別＆參考、封裝、繼承、介面與抽象類別、列舉＆群集等等。
也有介紹到LINQ的部分，這個我比較常用，可以讓我在實務上更快速的使用及理解。
寫書方式非常活潑，而且每頁都有一堆小註記，這些小註記也非常有幫助，
書的末頁也有外文書的優良習慣，就是照字母A~Z列出所有關鍵字和其頁碼，所以要複習之前的東西非常方便。

不過這本書也有缺點，就是內容太多，
這本也不適合當工具書，因為他的排版真的是太特別了(這既是優點也是缺點)，如果當工具書查指令一定會瘋掉。

